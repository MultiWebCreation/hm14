<!--header-->
<header>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="index.php">IT Hillel</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <!--navbar-->
                    <ul class="navbar-nav">
                        <?php foreach ($menu as $item) : ?>
                            <li class="nav-item <?=$item['class']?>">
                                <a class="nav-link" href="<?=$item['link']?>"><?=$item['title']?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <!--end navbar-->
                </div>
            </nav>
        </div>
    </div>
</header>
<!--end header-->