<?php

// массив для формирования пунктов меню, class="active" - для активного пункта меню
$menu = [
    ['title' => 'Главная', 'link' => 'index.php', 'class' => 'active'],
    ['title' => 'Результат', 'link' => 'new.php', 'class' => ''],
    ['title' => 'Контакты', 'link' => 'contacts.php', 'class' => ''],
];

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Главная - Домашнее задание №11</title>
    <meta name="description" content="Домашнее задание №11. Компьютерная школа HILLEL">
    <meta name="keywords" content="Базы данных, PDO, Exceptions  HILLEL, компьютерная школа, Одесса-мама">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php require_once 'blocks/header.php'; ?>

<!--content-->
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Поиск с помощью регулянных выражений</h1>
                <h2>Домашнее задание №14</h2>
                <span class="date">Добавлено: 19.02.2019 01:11</span>
                <p>Есть два массива.</p>
                <p>Массив слов:</p>
                <pre>$searchWords = array(‘php’,’html’,’интернет’,’Web');</pre>
                <pre>
                    $searchStrings = array(
                    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.’,
                    ‘PHP - это распространенный язык программирования с открытым исходным кодом.’,
                    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
                    );
                </pre>
                <p>Написать функцию, которая примет эти массивы как аргументы и в цикле с помощью регулярных выражений проверит какие слова из массива <b>$searchWords</b> содержатся в предложениях массива <b>$searchStrings</b>. Результат должен быть вида:</p>
                <ul class="list">
                    <li>В предложении №1 есть слова: <b>интернет</b>.</li>
                    <li>В предложении №2 есть слова: <b>php</b>.</li>
                    <li>В предложении №3 есть слова: <b>php</b>, <b>Web</b>, <b>html</b>.</li>
                </ul>
                <p>Сдаем в виде ссылки на Bitbucket</p>
                <p>Домашнее задание сдать <b>ZIP-архивом</b> с названием hw_(номер домашнего задания)_(Ваша фамилия).zip , например: <b>hw_14_Petin.zip</b></p>
                <h3>Материалы для выполнения:</h3>
                <ul class="upload">
                    <li><a download href="upload/lesson18.zip">Архив урока №14</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--end content-->

<?php include 'blocks/footer.php'; ?>

</body>
</html>