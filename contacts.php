<?php
$phone = '+38 (093) 421-32-85'; //Задаем номер телефона через переменную $phone
$phoneLink = '+380934213285'; //Задаем ссылку на номер телефона для звонка при нажатии через переменную $phone_link
$email = 'argirov.igor@gmail.com'; //Задаем почтовый ящик через переменную $email
$address = '65000, ул. Канатная, 22, г. Одесса'; //Задаем адрес через переменную $address

$menu = [
    ['title' => 'Главная', 'link' => 'index.php', 'class' => ''],
    ['title' => 'Результат', 'link' => 'new.php', 'class' => ''],
    ['title' => 'Контакты', 'link' => 'contacts.php', 'class' => 'active'],
];

?>


<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Контакты - Домашнее задание №11</title>
    <meta name="description" content="Домашнее задание №11. Компьютерная школа HILLEL">
    <meta name="keywords" content="Базы данных, PDO, Exceptions  HILLEL, компьютерная школа, Одесса-мама">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php require_once 'blocks/header.php'; ?>

<!--content-->
<section id="content">
    <div class="container">
        <!--info-->
        <div class="row info">
            <div class="col-md-12">
                <h1>Контактная информация</h1>
            </div>
            <div class="col-md-4 text-center one-info">
                <i class="fas fa-phone-volume"></i>
                <span><a href="tel:<?=$phoneLink?>"><?=$phone?></a></span>
            </div>
            <div class="col-md-4 text-center one-info">
                <i class="fas fa-map-marker-alt"></i>
                <span><?=$address?></span>
            </div>
            <div class="col-md-4 text-center one-info">
                <i class="fas fa-at"></i>
                <span><a href="mailto:<?=$email?>"><?=$email?></a></span>
            </div>
        </div>
        <!--end info-->
        <!--form-->
        <div class="row">
            <div class="col-md-12">
                <h1>Форма обратной связи</h1>
                <form action="contacts.php" id="contacts">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1">Ваше имя (*)</label>
                            <input name="name" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Введите Ваше имя" value="" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput2">Номер телефона</label>
                            <input name="phone" type="text" class="form-control" id="exampleFormControlInput2" placeholder="Введите номер телефона"  value="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput3">Ваш email (*)</label>
                            <input name="email" type="email" class="form-control" id="exampleFormControlInput3" placeholder="Введите email адрес"  value=""  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Сообщение (*)</label>
                        <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Введите текст сообщения"  required></textarea>
                    </div>
                    <p class="warning">(*) - обязательные поля для заполнения</p>
                    <button type="submit" class="btn btn-primary">Отправить письмо</button>
                </form>
                <p class="text-justify">Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum</p>
                <p class="text-justify">Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum</p>
            </div>
        </div>
        <!--end form-->
    </div>
</section>
<!--end content-->

<?php include 'blocks/footer.php'; ?>

</body>
</html>