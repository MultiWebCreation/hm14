<?php

// массив для формирования пунктов меню, class="active" - для активного пункта меню
$menu = [
    ['title' => 'Главная', 'link' => 'index.php', 'class' => ''],
    ['title' => 'Результат', 'link' => 'new.php', 'class' => 'active'],
    ['title' => 'Контакты', 'link' => 'contacts.php', 'class' => ''],
];

$searchStrings = [
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML',
];

$searchWords = [
    'php',
    'html',
    'интернет',
    'Web',
];

require_once 'lib.php';

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Главная - Домашнее задание №14</title>
    <meta name="description" content="Домашнее задание №14. Компьютерная школа HILLEL">
    <meta name="keywords" content="Базы данных, PDO, Exceptions  HILLEL, компьютерная школа, Одесса-мама">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php require_once 'blocks/header.php'; ?>

<!--content-->
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php words($searchStrings, $searchWords); ?>
            </div>
        </div>
    </div>
</section>
<!--end content-->

<?php include 'blocks/footer.php'; ?>

</body>
</html>